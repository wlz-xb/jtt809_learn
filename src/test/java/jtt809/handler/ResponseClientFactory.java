package jtt809.handler;

import com.sjx.jtt809.pojo.Response;
import com.sjx.jtt809.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;
import jtt809.pojo.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 响应类创建工厂
 */
public class ResponseClientFactory {

    private static Map<Integer, Class<? extends Response>> responseMap = new HashMap<Integer, Class<? extends Response>>();

    /**
     * 业务数据类型与bean对应的map集合
     */
    static {
        responseMap.put(ConstantJtt809Util.UP_CONNECT_REP, ResponseClientJtt809_0x1002.class);
        responseMap.put(ConstantJtt809Util.UP_LINKTEST_RSP, ResponseClientJtt809_0x1006.class);
        responseMap.put(ConstantJtt809Util.DOWN_CONNECT_REQ, ResponseClientJtt809_0x9001.class);
        responseMap.put(ConstantJtt809Util.DOWN_LINKTEST_REQ, ResponseClientJtt809_0x9005.class);
        responseMap.put(ConstantJtt809Util.DOWN_TOTAL_RECY_BACK_MSG, ResponseClientJtt809_0x9101.class);
    }

    /**
     * 创建响应数据包
     *
     * @param msgId 业务数据类型
     * @param buf   数据包
     * @return
     */
    public static Response createResponse(int msgId, ByteBuf buf) {
        // 根据业务
        Class<? extends Response> cls = responseMap.get(msgId);

        if (cls == null) {
            throw new RuntimeException("不支持的应答数据");
        }

        try {
            Response response = cls.newInstance();
            response.decode(buf);

            return response;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}
