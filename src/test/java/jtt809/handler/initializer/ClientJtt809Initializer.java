package jtt809.handler.initializer;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;
import jtt809.codec.ClientDecoderJtt809;
import jtt809.codec.ClinetEncoderJtt809;
import jtt809.handler.PrimaryLinkClientJtt809Handler;

import java.util.concurrent.TimeUnit;

public class ClientJtt809Initializer extends ChannelInitializer<SocketChannel> {

    private String ip;

    private int port;

    private String downLinkIp;

    private int downLinkPort;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDownLinkIp() {
        return downLinkIp;
    }

    public void setDownLinkIp(String downLinkIp) {
        this.downLinkIp = downLinkIp;
    }

    public int getDownLinkPort() {
        return downLinkPort;
    }

    public void setDownLinkPort(int downLinkPort) {
        this.downLinkPort = downLinkPort;
    }

    public ClientJtt809Initializer(String ip, int port, String downLinkIp, int downLinkPort) {
        this.ip = ip;
        this.port = port;
        this.downLinkIp = downLinkIp;
        this.downLinkPort = downLinkPort;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline channelPipeline = ch.pipeline();

        // 打印日志信息
//        channelPipeline.addLast("loging", new LoggingHandler(LogLevel.INFO));

        // 解码 和 编码
        channelPipeline.addLast("decoder", new ClientDecoderJtt809());
        channelPipeline.addLast("encoder", new ClinetEncoderJtt809());

        // 心跳检测
        channelPipeline.addLast("timeout", new IdleStateHandler(0, 140, 0, TimeUnit.SECONDS));

        // 客户端的逻辑
        channelPipeline.addLast("handler", new PrimaryLinkClientJtt809Handler(ip, port, downLinkIp, downLinkPort));
    }
}