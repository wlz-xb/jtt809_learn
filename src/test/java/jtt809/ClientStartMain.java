package jtt809;

import cn.hutool.core.thread.ThreadUtil;

public class ClientStartMain {

    public static void main(String[] args) throws Exception {
        // 启动下级平台客户端
        ThreadUtil.execute(new PrimaryLinkClientManager("127.0.0.1", 8080, "127.0.0.1", 9090));

        Thread.sleep(2000);

        // 启动下级平台服务端
        ThreadUtil.execute(new SlaveLinkClientManager("127.0.0.1", 9090));

        Thread.sleep(10000);

        // 启动下级平台客户端
        ThreadUtil.execute(new PrimaryLinkClientManager("127.0.0.1", 8081, "127.0.0.1", 9091));

        Thread.sleep(2000);

        // 启动下级平台服务端
        ThreadUtil.execute(new SlaveLinkClientManager("127.0.0.1", 9091));
    }
}
