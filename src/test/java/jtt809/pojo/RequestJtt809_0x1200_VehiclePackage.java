package jtt809.pojo;

import com.sjx.jtt809.pojo.BasePackage;
import com.sjx.jtt809.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;

/**
 * 车辆信息基本类
 */
public abstract class RequestJtt809_0x1200_VehiclePackage extends BasePackage {

    /**
     * 车辆基本信息数据长度
     */
    public static final short VEHICLE_MSG_FIX_LENGTH = 28;

    /**
     * 车牌号
     */
    private String vehicleNo;

    /**
     * 车辆颜色，按照 JT/T415-2006中 5.4.12 的规定
     * 1 - 蓝色
     * 2 - 黄色
     * 3 - 黑色
     * 4 - 白色
     * 9 - 其他
     */
    private RequestJtt809_0x1200_VehicleColor vehicleColor;

    /**
     * 子业务类型标识
     */
    private short dataType;

    /**
     * 后续数据长度
     */
    protected int dataLength;

    public RequestJtt809_0x1200_VehiclePackage(int dataType) {
        super(ConstantJtt809Util.UP_EXG_MSG);

        this.dataType = (short) dataType;
        this.msgBodyLength = VEHICLE_MSG_FIX_LENGTH + dataLength;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public RequestJtt809_0x1200_VehicleColor getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(RequestJtt809_0x1200_VehicleColor vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public short getDataType() {
        return dataType;
    }

    public void setDataType(short dataType) {
        this.dataType = dataType;
    }

    public int getDataLength() {
        return dataLength;
    }

    public void setDataLength(int dataLength) {
        this.dataLength = dataLength;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        ByteBuf baseBuf = Unpooled.buffer(getMsgBodyLength());
        // 车牌号 21 byte
        try {
            baseBuf.writeBytes(getBytesWithLengthAfter(21, getVehicleNo().getBytes("GBK")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 车牌颜色：注意不是车身颜色 1 byte
        baseBuf.writeByte((byte) getVehicleColor().getRet());
        // 子业务码 2 byte
        baseBuf.writeShort(getDataType());
        // 后续数据长度 4 byte
        baseBuf.writeInt(getDataLength());
        // 具体车辆信息数据体加密啊
        encodeDataImpl(baseBuf);

        // 写入最终数据包
        buf.writeBytes(baseBuf);
    }

    /**
     * 具体车辆数据实现方法
     *
     * @param buf
     */
    protected abstract void encodeDataImpl(ByteBuf buf);

    /**
     * 将16进制字符串转换为byte[]
     *
     * @param hex
     * @return
     */
    protected byte[] hexStringToByte(String hex) {
        if (hex == null || hex.trim().equals("")) {
            return new byte[0];
        }

        byte[] bytes = new byte[hex.length() / 2];
        for (int i = 0; i < hex.length() / 2; i++) {
            String subStr = hex.substring(i * 2, i * 2 + 2);
            bytes[i] = (byte) Integer.parseInt(subStr, 16);
        }

        return bytes;
    }

    /**
     * 格式化经纬度,保留六位小数
     *
     * @param needFormat
     * @return
     */
    protected int formatLonLat(Double needFormat) {
        NumberFormat numFormat = NumberFormat.getInstance();
        numFormat.setMaximumFractionDigits(6);
        numFormat.setGroupingUsed(false);
        String fristFromat = numFormat.format(needFormat);
        Double formatedDouble = Double.parseDouble(fristFromat);
        numFormat.setMaximumFractionDigits(0);
        String formatedValue = numFormat.format(formatedDouble * 1000000);
        return Integer.parseInt(formatedValue);
    }
}
