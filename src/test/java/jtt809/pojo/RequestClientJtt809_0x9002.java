package jtt809.pojo;

import com.sjx.jtt809.pojo.BasePackage;
import com.sjx.jtt809.util.ConstantJtt809Util;
import io.netty.buffer.ByteBuf;

/**
 * 从链路连接应答信息
 * 链路类型:从链路。
 * 消息方问:下级平台往上级平台。
 * 业务数据类型标识:DOWN_CONNNECT_RSP。
 * 描述：下级平台作为服务器端向上级平台客户端返回从链路连接应答消息，上级平台在接收到该应答消息结果后，根据结果进行链路连接处理
 */
public class RequestClientJtt809_0x9002 extends BasePackage {

    private RequestClientJtt809_0x9002_Result result;

    public RequestClientJtt809_0x9002() {
        super(ConstantJtt809Util.DOWN_CONNECT_RSP);
        this.msgBodyLength = 1;
    }

    public RequestClientJtt809_0x9002_Result getResult() {
        return result;
    }

    public void setResult(RequestClientJtt809_0x9002_Result result) {
        this.result = result;
    }

    @Override
    protected void encodeImpl(ByteBuf buf) {
        // 1 byte
        buf.writeByte(getResult().getRet());
    }
}
