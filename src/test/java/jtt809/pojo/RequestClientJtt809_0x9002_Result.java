package jtt809.pojo;

public enum RequestClientJtt809_0x9002_Result {

    /**
     * 成功
     */
    SUCCESS(0x00,"成功"),

    /**
     * VERIFY_CODE 错误
     */
    VERIFY_CODE_ERROR(0x01,"VERIFY_CODE 错误"),

    /**
     * 资源紧张，稍后再连接（已经占用）
     */
    RESOURCE_CRISIS(0x02, "资源紧张，稍后再连接（已经占用）"),

    /**
     * 其他
     */
    OTHER(0x03, "其他");

    private int ret;

    private String msg;

    RequestClientJtt809_0x9002_Result(int ret,String msg) {
        this.ret = ret;
        this.msg = msg;
    }

    public int getRet() {
        return ret;
    }

    public void setRet(int ret) {
        this.ret = ret;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
