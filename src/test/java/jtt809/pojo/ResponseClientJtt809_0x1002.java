package jtt809.pojo;

import com.sjx.jtt809.pojo.Response;
import io.netty.buffer.ByteBuf;

public class ResponseClientJtt809_0x1002 extends Response {

    /**
     * 上级平台是否登录成功
     */
    private static boolean isLoginFlagFromUpPlatform = false;

    /**
     * 最终存储的校验码
     */
    private static long resultVerifyCode = -1;

    /**
     * 验证结果，定义如下：
     * 0x00:成功;
     * 0x01:IP 地址不正确；
     * 0x02:接入码不正确；
     * 0x03:用户没用注册；
     * 0x04:密码错误；
     * 0x05:资源紧张，稍后再连接(已经占用）；
     * 0x06：其他。
     */
    private byte result;

    /**
     * 校验码
     */
    private long verifyCode;

    public byte getResult() {
        return result;
    }

    public void setResult(byte result) {
        this.result = result;
    }

    public long getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(long verifyCode) {
        this.verifyCode = verifyCode;
    }

    public static boolean isIsLoginFlagFromUpPlatform() {
        return isLoginFlagFromUpPlatform;
    }

    public void setIsLoginFlagFromUpPlatform(boolean isLoginFlagFromUpPlatform) {
        ResponseClientJtt809_0x1002.isLoginFlagFromUpPlatform = isLoginFlagFromUpPlatform;
    }

    public static long getResultVerifyCode() {
        return resultVerifyCode;
    }

    public void setResultVerifyCode(long resultVerifyCode) {
        ResponseClientJtt809_0x1002.resultVerifyCode = resultVerifyCode;
    }

    @Override
    protected void decodeImpl(ByteBuf buf) {
        this.result = buf.readByte();
        this.verifyCode = buf.readUnsignedInt();
    }
}
