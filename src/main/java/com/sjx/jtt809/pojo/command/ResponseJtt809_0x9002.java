package com.sjx.jtt809.pojo.command;

import com.sjx.jtt809.pojo.Response;
import io.netty.buffer.ByteBuf;

/**
 * 从链路连接应答信息
 * 链路类型:从链路。
 * 消息方问:下级平台往上级平台。
 * 业务数据类型标识:DOWN_CONNNECT_RSP。
 * 描述：下级平台作为服务器端向上级平台客户端返回从链路连接应答消息，上级平台在接收到该应答消息结果后，根据结果进行链路连接处理
 */
public class ResponseJtt809_0x9002 extends Response {

    /**
     * 下级平台登录结果
     */
    private static boolean isLoginFlagFromDownPlatform;

    private byte result;

    public byte getResult() {
        return result;
    }

    public void setResult(byte result) {
        this.result = result;
    }

    public static boolean isIsLoginFlagFromDownPlatform() {
        return isLoginFlagFromDownPlatform;
    }

    public void setIsLoginFlagFromDownPlatform(boolean isLoginFlagFromDownPlatform) {
        ResponseJtt809_0x9002.isLoginFlagFromDownPlatform = isLoginFlagFromDownPlatform;
    }

    @Override
    protected void decodeImpl(ByteBuf buf) {
        this.result = buf.readByte();
    }
}
