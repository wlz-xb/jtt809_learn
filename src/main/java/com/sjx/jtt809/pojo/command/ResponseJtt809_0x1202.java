package com.sjx.jtt809.pojo.command;

import com.sjx.jtt809.util.HexBytesUtil;
import io.netty.buffer.ByteBuf;

/**
 * 车辆动态信息交换业务
 * 链路类型：主链路
 * 消息方向：下级平台往上级平台
 * 业务数据类型标识：UP_EXG_MSG
 * 描述：下级平台向上级平台发送车辆动态信息交换业务数据包
 *
 * 具体描述：
 * 实时上传车辆定位信息消息
 * 子业务类型标识：UP_EXG_MSG_REAL_LOCATION
 * 描述：主要描述车辆的实时定位信息
 */
public class ResponseJtt809_0x1202 extends ResponseJtt809_0x1200_VehiclePackage {

    /**
     * 该字段标识传输的定位信息是否使用国家测绘局批准的地图保密插件进行加密。
     * 加密标识：1-已加密，0-未加密。
     */
    private byte encrypt;

    /**
     * 日月年（dmyy），年的表示是先将年转换成 2 为十六进制数，如 2009 标识为 0x070xD9.
     * 时分秒（hms）
     */
    // 日
    private int day;

    // 月
    private int month;

    // 年
    private int year;

    // 时
    private int hour;

    // 分
    private int minute;

    // 秒
    private int second;

    /**
     * 经度，单位为 1*10^-6 度。
     */
    private double lon;

    /**
     * 纬度，单位为 1*10^-6 度。
     */
    private double lat;

    /**
     * 速度，指卫星定位车载终端设备上传的行车速度信息，为必填项。单位为千米每小时（km/h）。
     */
    private int vec1;

    /**
     * 行驶记录速度，指车辆行驶记录设备上传的行车速度信息，为必填项。单位为千米每小时（km/h）。
     */
    private int vec2;

    /**
     * 车辆当前总里程数，值车辆上传的行车里程数。单位为千米（km）。
     */
    private long vec3;

    /**
     * 方向，0-359，单位为度（。），正北为 0，顺时针。
     */
    private int direction;

    /**
     * 海拔高度，单位为米（m）
     */
    private int altitude;

    /**
     * 车辆状态，二进制表示，B31B30B29。。。。。。B2B1B0.具体定义按照 JT/T808-2011 中表 17 的规定
     * 位            状态
     * 0             0：ACC关；1：ACC开
     * 1             0：未定位；1：定位
     * 2             0：北纬；1：南纬
     * 3             0:东经；1：西经
     * 4             0：运营桩体；1：停运状态
     * 5             0：经纬度未经保密插件加密；1：经纬度已经保密插件加密
     * 6-9           保留
     * 10            0：车辆油路正常；1：车辆油路断开
     * 11            0：车辆电路正常；1：车辆电路断开
     * 12            0:车门解锁；1：车门加锁
     * 13-31         保留
     */
    private long state;

    /**
     * 报警状态，二进制表示，0 标识正常，1 表示报警：B31B30B29 。。。。。。 B2B1B0. 具 体定义按照 JT/T808-2011 中表 18 的规定
     *
     * 位	     定 义	                            处理说明
     * 0        1：紧急报警，触动报警开关后触发	    收到应答后清零
     * 1        1：超速报警	                        标志维持至报警条件解除
     * 2        1：疲劳驾驶	                        标志维持至报警条件解除
     * 3        1：预警	                            收到应答后清零
     * 4        l：GNSS模块发生故障                  标志维持至报警条件解除
     * 5        1：GNSS天线未接或被剪断	            标志维持至报警条件解除
     * 6        l：GNSS天线短路	                    标志维持至报警条件解除
     * 7        1：终端主电源欠压                    标志维持至报警条件解除
     * 8        1：终端主电源掉电                    标志维持至报警条件解除
     * 9        1：终端LCD或显示器故障               标志维持至报警条件解除
     * 10       1：TTS模块故障                       标志维持至报警条件解除
     * 11       1：摄像头故障                        标志维持至报警条件解除
     * 12-17    保留
     * 18       1：当天累计驾驶超时	                标志维持至报警条件解除
     * 19       1：超时停车                          标志维持至报警条件解除
     * 20       1：进出区域                          收到应答后清零
     * 21       1：进出路线                          收到应答后清零
     * 22       1：路段行驶吋间不足/过长              收到应答后清零
     * 23       1：路线偏离报警                      标志维持至报警条件解除
     * 24       1：车辆VSS故障                       标志维持至报警条件解除
     * 25       1：车辆油量异常                      标志维持至报警条件解除
     * 26       1：车辆被盗（通过车辆防盗器）         标志维持至报警条件解除
     * 27       1：车辆非法点火                      收到应答后清零
     * 28       1：车辆非法位移	                    收到应答后清零
     * 29       1：碰撞侧翻报警                      标志维持至报警条件解除
     * 30-31	保留
     */
    private long alarm;

    public byte getEncrypt() {
        return encrypt;
    }

    public void setEncrypt(byte encrypt) {
        this.encrypt = encrypt;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public int getVec1() {
        return vec1;
    }

    public void setVec1(int vec1) {
        this.vec1 = vec1;
    }

    public int getVec2() {
        return vec2;
    }

    public void setVec2(int vec2) {
        this.vec2 = vec2;
    }

    public long getVec3() {
        return vec3;
    }

    public void setVec3(long vec3) {
        this.vec3 = vec3;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public long getState() {
        return state;
    }

    public void setState(long state) {
        this.state = state;
    }

    public long getAlarm() {
        return alarm;
    }

    public void setAlarm(long alarm) {
        this.alarm = alarm;
    }

    @Override
    protected void decodeDataImpl(ByteBuf buf) {
        this.encrypt = buf.readByte();

        this.day = (int) buf.readByte();
        this.month = (int) buf.readByte();
        byte[] yearBytes = new byte[2];
        buf.readBytes(yearBytes);
        this.year = Integer.parseInt(HexBytesUtil.bytesToHex(yearBytes),16);

        this.hour = buf.readByte();
        this.minute = buf.readByte();
        this.second = buf.readByte();

        this.lon = (buf.readUnsignedInt() / 1000000d);
        this.lat = (buf.readUnsignedInt() / 1000000d);

        this.vec1 = buf.readUnsignedShort();
        this.vec2 = buf.readUnsignedShort();
        this.vec3 = buf.readUnsignedInt();

        this.direction = buf.readUnsignedShort();

        this.altitude = buf.readUnsignedShort();

        this.state = buf.readUnsignedInt();

        this.alarm = buf.readUnsignedInt();
    }
}
